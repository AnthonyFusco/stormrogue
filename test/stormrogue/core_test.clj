(ns stormrogue.core-test
  (:require [clojure.test :refer :all]
            [stormrogue.core :refer :all]))


(deftest prime-test

  (testing "initial"
    (is (= (primes 1) [2])))

  (testing "2"
    (is (= (primes 2) [2 3])))

  (testing "5"
    (is (= (primes 5) [2 3 5 7 11])))

  (testing "last 100"
    (is (= (last (primes 100)) 541)))
  )
