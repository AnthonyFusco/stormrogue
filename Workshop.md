# Multiplayer dungeon crawler engine
- D&D like rules
- set of tools to create levels (like dungeons), dialog trees, monsters, abilities, etc
- multiplayer, probably as an simple API
- enemy AIs can be scripted. The role of the enemies can be played by a player (like the DM in D&D)
- decoupled gameplay engine and UI

# Concept
The game become an ultra advanced dungeon builder, Dungeon Keeper with customisation, multiplayer
and programming aspects. There is 2 gameplay, the building of a dungeon/level and the players who play it.

The same principles applies from D&D, you make a dungeon for your friends that they can beat.
# MindMap
escape game
  - create your puzzles
  
D&D
  - every rpg encounter is a puzzle
  - NetHack
  
dungeon keeper
  - resource management
  - limited resource for a dungeon ?
  - the goal is for a group of players to accomplish all the goals set by the level
  
Learn to program with a fun subject
  - a simple AI, trap, ...
  - rpgmaker

leaderboard
  - a separated leaderboard for every level
  - measure win rate ?
  - note by other players ?
    - the levels leaderboard is a ranking of the levels by popularity
  - leaderboard for players and for DMs ?
  - Point system for the players ?
    - each goal of the level gives a certain numbers of points

enemy AI
  - FF12 Gambits ?

Possibility to make your own UI, enemies, player class, etc

Maybe remove the programming part completely and focus on using building block and visual programming (Scratch ?)
