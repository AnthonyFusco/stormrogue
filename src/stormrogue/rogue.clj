(ns stormrogue.rogue)

(require '[clojure.repl :refer :all])

;(defn format-rooms
;  [room]
;  (if (vector? (first room))
;    "A hallway is going further down"
;    (str (:description (first room)))))
;
;(defn show-dungeon
;  [dungeon]
;  (println "where do you want to go?")
;  (apply println (map-indexed #(str %1 " - " %2 "\n") (map format-rooms dungeon))))
;
;(defn tick-game
;  [dungeon input]
;  (get dungeon input))
;
;(defn run []
;  (let [empty-room {:description "A small, cold and empty room"}
;        trapped-room {:description "There is a treasure sitting in the middle of this otherwise empty room"}
;        monster-room {:description "A goblin is sleeping alone"}]
;    (loop [dungeon (vector (vector empty-room) (vector trapped-room) (vector (vector empty-room) (vector monster-room)))]
;      (if (vector? dungeon)
;        (do
;          (show-dungeon dungeon)
;          (recur (tick-game dungeon (read-string (read-line)))))
;        (println "The End")))))


;(def d {:dungeon [[0 [2 [3 [4] [5]]]] [1]]
;        :view    0
;        :score   0})


;(def move-history 'dungeon)
;
;(defn go-forward [move-history choice]
;  (list move-history choice))
;
;(defn go-back [move-history]
;  (first move-history))
;
;(defn view [move-history]
;  (let [v (eval move-history)]
;    (if (vector? v)
;      (map #(if (vector? %) :path {:room %}) v)
;      {:room v})))
;
;(defn room? [view]
;  (some? (:room view)))
;
;;; bot
;
;(defn next-random [view]
;  (rand-int (count view)))
;
;(defn next-random-not-visited [view mem]
;  (let [rooms (into #{} (map :room (filter room? view)))
;        not-visited (clojure.set/difference rooms mem)]
;    (println "not visited !! " not-visited)
;    (if (empty? not-visited)
;      :go-back                                              ;; todo: for now if only path then :go-back
;      (.indexOf (map :room view) (first not-visited)))))
;
;(defn bot
;  "n => number of unique rooms to visit before stopping,
;     must be <= to the total of rooms available or it loops infinitely"
;  [dungeon n]
;  (loop [move-history '(dungeon 0)
;         mem #{}]
;    (let [v (view move-history)
;          next (next-random-not-visited v mem)]
;      (println "visited: " mem "\n"
;               "history: " move-history "\n"
;               "view: " v "\n")
;      (if (= n (count mem))
;        (print "I'm done, staying in " v)
;        (if (= :go-back next)
;          (recur (go-back move-history) (if (some? (:room v)) (conj mem (:room v)) mem))
;          (recur (go-forward move-history next) mem))))))


;; big +10hp
;; fat +5hp
;; skinny -5hp
;; sabretooth +10atk
;; strong +5atk
;; timid -5atk
(def rooms [{:desc "monster"} {:desc "tunnel"} {:desc "treasure"}])
(def dungeon [
              {:access [1 3 5] :room 0}
              {:access [0 2] :room 0}
              {:access [1 3 4] :room 1}
              {:access [2 4 0] :room 2}
              {:access [2 3] :room 2}
              {:access [0] :room 2}
              ])

;; bot

(defn choose-next [visited current-node]
  (let [not-visited (clojure.set/difference (set (:access current-node)) visited)]
    (if (empty? not-visited)
      :go-back
      (first not-visited))))

(defn bot [dungeon]
  (loop [path '()
         visited #{}
         current-node-id 0
         history []]
    (let [current-node (dungeon current-node-id)
          next (choose-next visited current-node)]
      (println "history: " history "\n"
               "visited: " visited "\n"
               "path: " path "\n"
               "current node: " current-node)
      (if (= (count dungeon) (count visited))
        (println "I'm done, staying in " current-node "\nhistory:" (conj history current-node-id))
        (if (= :go-back next)
          (recur (rest path) visited (first (rest path)) (conj history current-node-id))
          (recur (conj path next) (conj visited next) next (conj history current-node-id)))))))
(bot dungeon)

(def a-dungeon [
                {:access [1 2] :room 0}
                {:room 1}
                {:access [3 4 5] :room 2}
                {:room 3}
                {:room 4}
                {:access [6 7] :room 5}
                {:room 6}
                {:room 7}
                ])
(defn next-nodes [x] (mapv #(a-dungeon %) (:access x)))
(tree-seq #(vector? (:access %)) next-nodes (first a-dungeon))