(ns stormrogue.fonc)

;Implement isSorted , which checks whether an Array[A] is sorted according to a given comparison function:
;def isSorted[A](as: Array[A], ordered: (A,A) => Boolean): Boolean

(defn my-sorted?
  "Checks whether a vector is sorted according to a given comparison function"
  [xs ordered]
  (apply ordered xs))

(let [sorted [0 1 2 3 4]
      not-sorted [25 6 1]]
  (println (= true (my-sorted? sorted <)))
  (println (= false (my-sorted? not-sorted <))))

(defn mycomp
  [f g]
  (fn [& args] (f (g args))))

(defn set-head
  [h l]

  (let [[_ & t] l]
    (list h t)))


;(defn prime? [n]
;  (let [to-test (range 2 (+ 1 n))
;        factor-list (map #(= 0 (rem n %)) to-test)]
;    (= 1 (count (filter true? factor-list)))))
;
;(defn primes [n]
;  (take n (filter prime? (range))))
;(read-line)
;(println "primes")
;(time (last (doall (primes 1000))))
;(time (last (doall (primes 1100))))
;(time (last (doall (primes 1200))))
;(time (last (doall (primes 1210))))
;(time (last (doall (primes 1300))))
;;(println "  6000")
;;(time (primes 6000))
;;(time (primes 6000))
;(time (last (primes 100)))
;(def memo-prime? (memoize prime?))
;(defn memo-primes [n]
;  (take n (filter memo-prime? (range))))
;
;(println)
;(println)
;
;(println "memo-primes")
;
;(time (last (doall (memo-primes 1000))))
;(time (last (doall (memo-primes 1100))))
;(time (last (doall (memo-primes 1200))))
;(time (last (doall (memo-primes 1210))))
;(time (last (doall (memo-primes 1300))))
;(println "  6000")
;;(time (memo-primes 6000))
;;(time (memo-primes 6000))
;(take 10 (filter prime? (range)))
;
;(defn primes2 [len]
;  (loop [n 2
;         acc []]
;    (if (= (count acc) len)
;      acc
;      (recur (+ n 1)
;             (if (prime? n) (conj acc n) acc)))))
;
;
;(filter #(= (type %) (type 1)) ["b" :d 1 "a" 2])
;
;(defn add-type [acc val]
;  (let [tv (type val)]
;    (if (some #{tv} acc)
;      acc
;      (conj acc tv))))
;
;(add-type [] ["b" :d 1 "a" 2])
;(reduce add-type [] ["b" :d 1 "a" 2])
;
;(some #{(type 1)} [(type 1)])
;
;(defn my-group-by
;  [f coll]
;  (reduce
;    (fn [ret x]
;      (let [k (f x)]
;        (assoc ret k (conj (get ret k []) x))))
;    {} coll))
;
;(group-by type ["b" :d 1 "a" 2])
;(my-group-by type ["b" :d 1 "a" 2])



;(defn neighbours-of
;  [[x y]]
;  [[(- x 2) y]
;   [(+ x 2) y]
;   [x (- y 2)]
;   [x (+ y 2)]])
;
;
;(defn toto
;  [[x y & xs]]
;  (str x y "xs" xs))